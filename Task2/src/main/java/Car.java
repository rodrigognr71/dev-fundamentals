/*
2. Crear una clase que representeun tipo de dato "Car" la cual deberia tener las siguientes especificaciones
        1. variables de instancia: year (entero), brand (cadena), model (cadena)
        2. agrega tres metodos para obtener los valor de las variables
        3. crear un metodo sin tipo de retorno llamado "updateInformation" que actualicen los valores year, brand y model
        4. crear un metodo llamado "updateInformation" que actualicen los valores year y model
            pero solo pueda actualizar la informacion si el nuevo valor year es mayor al antiguo valor
            si la actualizacion se realiza correctamente, retornar TRUE si no FALSE
        5. crear 3 constructores
            1. acepta los argumentos year, brand y model
            2. acepta los argumentos brand y model - por defecto el año actual
            3. acepta el argumento brand - por defecto el año actual, por defecto el modelo "Generic"
        6. agregar un contador en la clase Car de cuantos autos fueron creados cada vez que instanciamos la clase Car
        7. sobre escribir el metodo toString() para retornar toda la informacion del auto
        8. en un metodo main crear 4 objetos del tipo Car, con informacion defenida por ti (cada objeto debe tener diferentes valores)
            imprimir toda la informacion de los 4 objetos en consola
            actualizar la informacion de 2 de los 4 objetos creados
            imprimir toda la informacion de los 4 objetos en consola
            imprimir el valor del contador en consola
 */
public class Car {
    private int year;
    private String brand;
    private String model;

    public Car(int year, String brand, String model) {
        this.year = year;
        this.brand = brand;
        this.model = model;
    }

    public Car(String brand, String model) {
        this.brand = "New";
        this.model = "2020";
    }

    public Car(int year, String model) {
        this.year = 2020;
        this.model = "Generic";
    }

    public void updateInformation(int year, String brand, String model) {
        this.year = year;
        this.brand = brand;
        this.model = model;
    }

    public void updateInformation(int yearUpdated, String modelUpdated){
        this.year = yearUpdated;
        this.model = modelUpdated;
    }

    public int getYear() {
        return year;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }
}
