
/*
1. Crear una clase que represente un tipo de dato "Circle" la cual deberia tener las siguientes especificaciones
        1. radius - variable de instancia decimal que no pueda cambiar su valor despues de ser asignada
        2. constructor - sin argumentos, asignar radius con el valor por defecto 5.0
        3. constructor - que acepte un argumento para definir el valor de la variable radius
        4. un metodo para obtener el valor de la variable radius
        5. un metodo que calcule el area del circulo
        6. en un metodo main, crea 2 objetos del tipo Circle, imprime en consola el resultado de los metodos del objetos
            output:
                Circle 1 radius: NN,NN
                Circle 1 area: NN, NN
                Circle 2 radius: NN,NN
                Circle 2 area: NN, NN
 */
public class Circle {
    private final double radius;

    public Circle() {
        this.radius = 5;
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public double getRadius(double radius) {
        return radius;
    }

    public double circleArea(double radius) {
        double area = Math.PI * radius * radius;
        return area;
    }

    public static void main(String[] args) {
        Circle circle = new Circle();
        Circle circle1 = new Circle(8);

        System.out.println("Circle 1 radius: " + circle.radius);
        System.out.println("Circle 1 area: " + circle.circleArea(circle.radius));
        System.out.println("Circle 2 radius: " + circle1.getRadius(8));
        System.out.println("Circle 2 area:" + circle1.circleArea(8));
    }
}
