import java.util.Scanner;

public class Exercise2 {
    public static void main(String[] ARGS)
    {
        Scanner teclado = new Scanner(System.in);
        int number,i,j;

        System.out.print("indica la tabla de multiplicar: ");
        number = teclado.nextInt();

        if(number <= 10) {
            for (i = 1; i <= number; i++) {
                for (j = 1; j <= 10; j++) {
                    System.out.println(i + " X " + j + " = " + i * j);
                }
                System.out.println();
            }
        }
        else System.out.println("Ingrese un numero menor a 10");
    }
}
