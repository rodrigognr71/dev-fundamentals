/*
Dado un arreglo de numeros decimales, imprimir el numero menor y el numero mayor.
    array:
        [0,0.5,-0.5,2.9,8.5,5.8,-9.9,1.7,8.1]
    output:
        Largest number: X
        Smallest number: Y
 */
public class Exercise4 {
    public static void main(String[] args) {
        double[] arr = new double[]{0, 0.5, -0.5, 2.9, 8.5, 5.8, -9.9, 1.7, 8.1};
        double largestNumber = arr[0];
        double smallestNumber = 0.0;
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > largestNumber) {
                largestNumber = arr[i - 1];
            } else {
                if(arr[i] < smallestNumber) {
                    smallestNumber = arr[i];
                }
            }
        }
        System.out.println("Numero mayor es: " + largestNumber);
        System.out.println("Numero menor es: " + smallestNumber);
    }
}
