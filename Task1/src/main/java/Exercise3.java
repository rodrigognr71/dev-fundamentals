/*
Dado un arreglo de numeros enteros contar la cantidad de numeros positivos, numeros negativos y ceros en el arreglo e imprimir el resultado
    array:
        [4,-5,0,0-6,7,23,-5,0,8,55,-1,1,5,0,-9]
    output:
        Positive numbers: X
        Negative numbers: Y
        Zero numbers: Z
 */
public class Exercise3 {
    public static void main (String[] args){
        int[] array = new int[]{4,-5,0,0,-6,7,23,-5,0,8,55,-1,1,5,0,-9};
        int positiveNumbers = 0;
        int negativeNumbers = 0;
        int zeroNumbers = 0;

        for (int num : array) {
            if(num > 0) positiveNumbers++;
            else{
                if(num < 0) negativeNumbers++;
                else zeroNumbers++;
            }
        }
        System.out.println("Numeros positivos: " + positiveNumbers);
        System.out.println("Numeros negativos: " + negativeNumbers);
        System.out.println("Numero cero: " + zeroNumbers);
    }

}
