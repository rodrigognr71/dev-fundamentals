import java.util.Scanner;

/*
5. Escribir un programa que imprima los digitos de un numero al revez, debe funcionar con cualquier numero natural del tipo int.
    input:
        12345
    output:
        54321
 */
public class Exercise5 {
    public static void main(String[] args){
        int num;
        int invertNum = 0;
        int rest;

        Scanner teclado = new Scanner(System.in);
        System.out.println("Ingrese numero entero");

        num = teclado.nextInt();
        teclado.close();

        while( num > 0 ) {
            rest = num % 10;
            invertNum = invertNum * 10 + rest;
            num /= 10;
        }
        System.out.println( "Número invertido: " + invertNum );
    }
}
