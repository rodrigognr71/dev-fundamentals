import java.util.Scanner;

/*
1. Escribir un programa que calcule la suma de los numeros del 1 al 20 e imprima el resultado.
        output:
        Result: X
*/
public class Exercise1 {
    public static void main(String[] args){
        while(true) {
            System.out.println("Escriba primero numero menor a 20");
            Scanner teclado = new Scanner(System.in);
            int numero1 = teclado.nextInt();

            System.out.println("Escriba primero numero menor a 20");
            int numero2 = teclado.nextInt();

            if (numero1 > 20 || numero2 > 20) {
                System.out.println("Los numeros deben ser menores a 20");
            } else {
                System.out.println("El resultado es: " + (numero1 + numero2));
            }
        }
    }
}
